import { BikerpitstopPage } from './app.po';

describe('bikerpitstop App', function() {
  let page: BikerpitstopPage;

  beforeEach(() => {
    page = new BikerpitstopPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
